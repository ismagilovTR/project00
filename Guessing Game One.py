# -*- coding: utf-8 -*-
import random

number = random.randint(1,9)
guess = 0
count = 0

print('Если хотите закончить, то введите - Выход.\n')
while guess != number and guess != "Выход":
    guess = input("Что выпало?\n").strip()
    
    if guess == "Выход":
        break
    
    guess = int(guess)
    count += 1
    
    if guess < number:
        print("Мало")
    elif guess > number:
        print("Много")
    else:
        print("В точку")
print("Угадал за столько ходов -\n",count)